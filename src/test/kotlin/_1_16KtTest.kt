import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{

    @Test
    fun checkResultNegativeNumber (){
        val expected = -24.0
        assertEquals(expected, transformacionEntero(-24.0))
    }

    @Test
    fun checkResultPositiveNumber (){
        val expected = 44.0
        assertEquals(expected, transformacionEntero(44.0))
    }

    @Test
    fun checkResultLongNumber (){
        val expected = 1550.0
        assertEquals(expected, transformacionEntero(1550.0))
    }
}