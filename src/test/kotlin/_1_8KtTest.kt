import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{

    @Test
    fun checkResultNegativeNumber (){
        val expected = -4.2
        assertEquals(expected, doblaElDecimal(-2.1))
    }

    @Test
    fun checkResultPositiveNumber (){
        val expected = 246.912
        assertEquals(expected, doblaElDecimal(123.456))
    }

    @Test
    fun checkResultLongNumber (){
        val expected = 200001.1
        assertEquals(expected, doblaElDecimal(100000.55))
    }
}