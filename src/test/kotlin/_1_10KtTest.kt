import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{

    @Test
    fun checkResultLongNumber (){
        val expected = "237462.5"
        assertEquals(expected, quinaMidaEsLaPizza(550.0))
    }

    @Test
    fun checkResultNumber0 (){
        val expected = "0.0"
        assertEquals(expected, quinaMidaEsLaPizza(0.0))
    }
    @Test
    fun checkResultNegativeNumber (){
        val expected = "ERROR!"
        assertEquals(expected, quinaMidaEsLaPizza(-1.0))
    }
}