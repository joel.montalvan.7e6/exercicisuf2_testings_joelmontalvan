import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{

    @Test
    fun checkResult1ClaseWithOStudents (){
        val expected = "24"
        assertEquals(expected, pupitres(22, 25, 0))
    }

    @Test
    fun checkResult3ClaseWithoutStudents (){
        val expected = "0"
        assertEquals(expected, pupitres(0, 0, 0))
    }

    @Test
    fun checkResult3ClaseWithSenarsStudents (){
        val expected = "41"
        assertEquals(expected, pupitres(31, 29, 19))
    }

    @Test
    fun checkResult3ClaseWithNegativeStudents (){
        val expected = "ERROR!"
        assertEquals(expected, pupitres(-31, -29, -19))
    }
}