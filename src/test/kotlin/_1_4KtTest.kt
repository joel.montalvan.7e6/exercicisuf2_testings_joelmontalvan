import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{

    @Test
    fun checkResultArea (){
        val expected = "15"
        assertEquals(expected, calculaElArea(3, 5))
    }

    @Test
    fun checkResultOneNegativeArea (){
        val expected = "144"
        assertEquals(expected, calculaElArea(-12, 12))
    }

    @Test
    fun checkResultNegativeArea (){
        val expected = "144"
        assertEquals(expected, calculaElArea(-12, -12))
    }
}