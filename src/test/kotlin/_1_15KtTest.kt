import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{

    @Test
    fun checkResultNegativeSecond (){
        val expected = "ERROR!"
        assertEquals(expected, segundoAgregado(-10))
    }

    @Test
    fun checkResultPositiveSecond (){
        val expected = "0"
        assertEquals(expected, segundoAgregado(59))
    }

    @Test
    fun checkResult60Second (){
        val expected = "ERROR!"
        assertEquals(expected, segundoAgregado(63))
    }
}