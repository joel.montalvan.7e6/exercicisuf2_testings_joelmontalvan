import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{

    @Test
    fun checkResultNegativeNumber (){
        val expected = "Després ve el -1.0"
        assertEquals(expected, numeroSeguent(-2.0))
    }

    @Test
    fun checkResultDecimalNumber (){
        val expected = "Després ve el -21.5"
        assertEquals(expected, numeroSeguent(-22.5))
    }

    @Test
    fun checkResultLongNumber (){
        val expected = "Després ve el 4001.0"
        assertEquals(expected, numeroSeguent(4000.0))
    }
}