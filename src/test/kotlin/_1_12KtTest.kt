import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{

    @Test
    fun checkResultNegativeTemperature (){
        val expected = 10.399999999999999
        assertEquals(expected, convertidorDeCelsiusHaciaFahrenheit(-12.0))
    }

    @Test
    fun checkResultPositiveTemperature (){
        val expected = 118.4
        assertEquals(expected, convertidorDeCelsiusHaciaFahrenheit(48.0))
    }

    @Test
    fun checkResultSmallTemperature (){
        val expected = 35.6
        assertEquals(expected, convertidorDeCelsiusHaciaFahrenheit(2.0))
    }
}