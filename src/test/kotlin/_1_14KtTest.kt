import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{

    @Test
    fun checkResultOnly1Comencal (){
        val expected = "48.55"
        assertEquals(expected, divisorDeCuentas(1.0, 48.55))
    }

    @Test
    fun checkResultALotComencalsWithSmallPrice (){
        val expected = "ERROR!"
        assertEquals(expected, divisorDeCuentas(-2.0, 14.0))
    }

    @Test
    fun checkResultNegativePrice (){
        val expected = "ERROR!"
        assertEquals(expected, divisorDeCuentas(10.0, -550.0))
    }
}