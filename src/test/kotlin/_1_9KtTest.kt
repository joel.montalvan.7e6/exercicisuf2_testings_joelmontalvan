import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{

    @Test
    fun checkResultNegativeNumbers (){
        val expected = 20.0
        assertEquals(expected, calculaElDescompte(-1000.0, -800.0))
    }

    @Test
    fun checkResultPostiveNumber (){
        val expected = 22.22222222222222
        assertEquals(expected, calculaElDescompte(540.0, 420.0))
    }

    @Test
    fun checkResultOneNegativeNumbers(){
        val expected = 20.0
        assertEquals(expected, calculaElDescompte(-1000.0, 800.0))
    }
}