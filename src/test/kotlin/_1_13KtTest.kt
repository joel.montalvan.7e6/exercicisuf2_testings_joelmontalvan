import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{

    @Test
    fun checkResultNegativeTemperature (){
        val expected = "La temperatura actual es -9.0"
        assertEquals(expected, temperaturaActual(-12.0, 3.0))
    }

    @Test
    fun checkResultNegativeTemperatureAndMinusTemperature (){
        val expected = "La temperatura actual es -15.0"
        assertEquals(expected, temperaturaActual(-12.0, - 3.0))
    }

    @Test
    fun checkResultNoIncreaseTemperature (){
        val expected = "La temperatura actual es 14.0"
        assertEquals(expected, temperaturaActual(14.0, 0.0))
    }
}