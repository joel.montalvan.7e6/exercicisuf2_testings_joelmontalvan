import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{
    @Test
    fun checkResultNegativeNumbers (){
        val expected = -4
        assertEquals(expected, sumaDosEnters(-2, -2))
    }

    @Test
    fun checkResultPositiveNumber (){
        val expected = 8
        assertEquals(expected, sumaDosEnters(2, 6))
    }

    @Test
    fun checkResultNegativeAndPositiveNumber (){
        val expected = 2
        assertEquals(expected, sumaDosEnters(5, -3))
    }
}