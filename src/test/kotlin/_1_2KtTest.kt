import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest {
    @Test
    fun checkResultNegativeNumber (){
        val expected = -4
        assertEquals(expected, doblaElNumero(-2))
    }

    @Test
    fun checkResultPositiveNumber (){
        val expected = 72
        assertEquals(expected, doblaElNumero(36))
    }

    @Test
    fun checkResultLongNumber (){
        val expected = 200000
        assertEquals(expected, doblaElNumero(100000))
    }
}