import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{

    @Test
    fun checkResultAllNegativeNumbers (){
        val expected = "ERROR!"
        assertEquals(expected, calculadoraDeVolum(-7.2, -3.1, -3.1))
    }

    @Test
    fun checkResultPositiveNumbers (){
        val expected = "69.19200000000001"
        assertEquals(expected, calculadoraDeVolum(7.2, 3.1, 3.1))
    }

    @Test
    fun checkResultAllNumbers0(){
        val expected = "0.0"
        assertEquals(expected, calculadoraDeVolum(0.0, 0.0, 0.0))
    }

}