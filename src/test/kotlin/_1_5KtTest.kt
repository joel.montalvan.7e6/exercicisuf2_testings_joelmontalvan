import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.lang.Double.NaN

internal class _1_5KtTest {

    @Test
    fun checkResultLongsNUmbers (){
        val expected = 9000000.0
        assertEquals(expected, operacioBoja(1000.0, 2000.0, 3000.0, 4000.0))
    }

    @Test
    fun checkResultAll0Numbers (){
        val expected = NaN
        assertEquals(expected, operacioBoja(0.0, 0.0, 0.0, 0.0))
    }

    @Test
    fun checkResultSumAll0AndModuleAllNumbers (){
        val expected = 0.0
        assertEquals(expected, operacioBoja(0.0, 0.0, 2.0, 4.0))
    }
}