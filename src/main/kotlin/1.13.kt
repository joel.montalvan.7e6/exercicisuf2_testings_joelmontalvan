import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/22
* TITLE: Quina tempreatura fa?
*/

fun main(){
    val scanner= Scanner(System.`in`)
    println("Introduce una temperatura en grados:")
    val temperatura = scanner.nextDouble()
    println("Cuanto ha aumentado la temperatura?")
    val aumento = scanner.nextDouble()
    val result = temperaturaActual(temperatura, aumento)


    println(result)
}

fun temperaturaActual (temperatura:Double, aumento:Double): String{
    return ("La temperatura actual es ${temperatura+aumento}")
}