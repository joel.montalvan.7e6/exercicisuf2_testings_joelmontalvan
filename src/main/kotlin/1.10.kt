import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/22
* TITLE: Quina és la mida de la meva pizza?
*/

fun main (){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introduce el diametro de la pizza:")
    val decimal = scanner.nextDouble()
    val result = quinaMidaEsLaPizza(decimal)

    println(result)
}

fun quinaMidaEsLaPizza (decimal:Double):String {
    return if (decimal < 0) ("ERROR!")
    else ("${3.14 * ((decimal * decimal)/ 4)}")
}