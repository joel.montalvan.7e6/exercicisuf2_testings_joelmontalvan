import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/19
* TITLE: Suma de dos nombres enters
*/


fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix una amplada:")
    val amplada = scanner.nextInt()
    println("Introdueix una llargada:")
    val llargada = scanner.nextInt()
    val result = calculaElArea(amplada, llargada)

    println(result)
}

fun calculaElArea (amplada:Int, llargada:Int):String{
    return if (amplada < 0 && llargada < 0) ("${(amplada * -1) * (llargada * -1)}")
    else if (amplada < 0) ("${(amplada * -1) * llargada}")
    else if (llargada < 0) ("${amplada * (llargada * -1)}")
    else ("${amplada * llargada}")
}
