import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/19
* TITLE: Operació boja
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce los alumnos que hay en las 3 clases:")
    val claseA = scanner.nextInt()
    val claseB = scanner.nextInt()
    val claseC = scanner.nextInt()
    val result = pupitres(claseA, claseB, claseC)

    println(result)
}

fun pupitres (claseA:Int, claseB:Int, claseC:Int):String {
    var count1 = 0
    var count2 = 0
    var count3 = 0

    if (claseA % 2 != 0){
        count1++
    }
    if (claseB % 2 != 0){
        count2++
    }
    if (claseC % 2 != 0){
        count3++
    }
    return if (claseA < 0 || claseB < 0 || claseC < 0) ("ERROR!")
    else ("${(claseA + claseB + claseC + count1 + count2 + count3) / 2}")
}