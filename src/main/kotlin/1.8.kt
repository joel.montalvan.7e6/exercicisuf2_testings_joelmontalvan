import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/21
* TITLE: Dobla el decimal
*/

fun main (){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introduce un decimal:")
    val decimal = scanner.nextDouble()
    val result = doblaElDecimal(decimal)

    println(result)
}

fun doblaElDecimal (decimal:Double):Double {
    return (decimal * 2)
}