import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/21
* TITLE: Calcula el descompte
*/

fun main (){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introduce el precio original:")
    val decimal1 = scanner.nextDouble()
    println("Introduce el precio final:")
    val decimal2 = scanner.nextDouble()
    val result = calculaElDescompte(decimal1, decimal2)

    println("$result %")
}

fun calculaElDescompte (decimal1:Double, decimal2:Double):Double {
    return if (decimal1 < 0 && decimal2 < 0) (((decimal1 * -1)-(decimal2 *-1)) * 100 / (decimal1 * -1))
    else if (decimal1 < 0) return (((decimal1 * -1)-decimal2)*100 / (decimal1 * -1))
    else if (decimal2 < 0) return ((decimal1 - (decimal2*-1))*100 / decimal1)
    else (((decimal1 - decimal2) * 100) / decimal1)
}