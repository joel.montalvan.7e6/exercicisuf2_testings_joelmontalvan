import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/19
* TITLE: Suma de dos nombres enters
*/


fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce los numeros que quieres sumar:")
    val numero1 = scanner.nextInt()
    val numero2 = scanner.nextInt()
    val result = sumaDosEnters(numero1, numero2)
    println(result)
}

fun sumaDosEnters (numero1:Int, numero2:Int):Int {
    return (numero1 + numero2)
}