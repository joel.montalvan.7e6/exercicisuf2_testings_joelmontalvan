import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/22
* TITLE: De Celsius a Fahrenheit
*/

fun main (){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix una temperatura en grau:")
    val celsius = scanner.nextDouble()
    val result = convertidorDeCelsiusHaciaFahrenheit(celsius)

    println("$result °F")
}

fun convertidorDeCelsiusHaciaFahrenheit (celsius:Double):Double {
    return ((celsius*1.8)+32)
}
