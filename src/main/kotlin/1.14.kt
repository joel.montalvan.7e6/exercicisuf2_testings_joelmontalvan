import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/22
* TITLE: Divisor de compte
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce cuantos comensales son:")
    val comensales = scanner.nextDouble()
    println("Cuanto es el coste de la cena?")
    val coste = scanner.nextDouble()
    val result = divisorDeCuentas(comensales, coste)

    println(result)
}

fun divisorDeCuentas (comensales:Double, coste:Double):String {
    return if (coste < 0 || comensales < 0) ("ERROR!")
    else ("${coste / comensales}")
}