import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/22
* TITLE: Calculadora de volum d'aire
*/

fun main (){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix una llargada:")
    val llargada = scanner.nextDouble()
    println("Introdueix una amplada:")
    val amplada = scanner.nextDouble()
    println("Introdueix una alçada:")
    val alçada = scanner.nextDouble()
    val result = calculadoraDeVolum(llargada, amplada, alçada)

    println(result)
}

fun calculadoraDeVolum (llargada:Double, amplada:Double, alçada:Double): String {
    return if (llargada < 0 || amplada < 0 || alçada < 0) ("ERROR!")
    else ("${llargada *amplada * alçada}")
}