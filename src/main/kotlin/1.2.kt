import java.util.*
/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/19
* TITLE: Dobla l'enter
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    val numero = scanner.nextInt()
    val result = doblaElNumero(numero)
    println(result)
}

fun doblaElNumero (numero:Int): Int {
    return (numero * 2)
}