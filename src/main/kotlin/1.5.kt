import java.util.*

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2023/09/19
* TITLE: Operació boja
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix 4 enters per fer la operació boja:")
    val numero1 = scanner.nextDouble()
    val numero2 = scanner.nextDouble()
    val numero3 = scanner.nextDouble()
    val numero4 = scanner.nextDouble()
    val result = operacioBoja(numero1, numero2, numero3, numero4)

    println(result)
}

fun operacioBoja (numero1:Double, numero2:Double, numero3:Double, numero4:Double):Double {
    return ((numero1 +  numero2)*(numero3 % numero4))
}