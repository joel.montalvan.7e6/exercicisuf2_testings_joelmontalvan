import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/09/22
* TITLE: Afegeix un segon
*/

fun main (){
    val scanner = Scanner(System.`in`)
    val segundo = scanner.nextInt()
    val result = segundoAgregado(segundo)

    println(result)
}

fun segundoAgregado (segundo: Int): String{
    return if (segundo < 0 || segundo >= 60) ("ERROR!")
    else ("${(segundo+1)%60}")
}